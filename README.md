# UnitTestDemo

## ch1 
建立 3A 單元測試

## ch2 
透過 tuple、factory 方法快速建立測試資料

## ch3.TestDouble
針對場景使用 dummy、fake、stub、spy、mock，可能會重複但請關注差異

## ch3
* NSub https://nsubstitute.github.io/ 
    * Stub 範例
    * Mock 範例
    * 簡化 SUT

## ch3.Moq
* Moq http://www.nudoq.org/#!/Projects/Moq




https://itenium.be/blog/dotnet/nsubstitute-vs-moq/

	


## TBD 未排入部分 :

* autofac

* Refactor by unit test
    * 針對 depend on private method
    * 針對 depond on static method
    * 針對 method not do one thing
    

* gitlab ci with cake


