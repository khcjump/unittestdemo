﻿using System.Collections.Generic;
using UnitTestDemo.Infrastructure.Helper;
using UnitTestDemo.Model;

namespace UnitTestDemo.Ch3
{
    public class ItemDA : IItemDA
    {
        private const string ItemSourcePath = "app/ItemSource.json";

        public List<Item> GetItems()
        {
            var itemSource = JsonHelper.LoadJson<ItemSource>(ItemSourcePath);

            return itemSource?.Items;
        }
    }
}