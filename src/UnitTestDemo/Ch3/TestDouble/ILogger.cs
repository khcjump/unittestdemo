﻿namespace UnitTestDemo.Ch3.TestDouble
{
    public interface ILogger
    {
        void WriteLog(string message);
    }
}