﻿using System;

namespace UnitTestDemo.Ch3.TestDouble
{
    public interface ITimeWrapper
    {
        DateTime Now { get; }
    }
}