﻿namespace UnitTestDemo.Ch3.TestDouble
{
    public class PriceCalculator : IPriceCalculator
    {
        public const string IPhone = "Iphone";

        public decimal GetDefaultPrice(string item)
        {
            return item == IPhone ? 10m : 0m;
        }

        public decimal GetExtendPrice(string item, int qty)
        {
            return GetDefaultPrice(item) * qty;
        }
    }
}