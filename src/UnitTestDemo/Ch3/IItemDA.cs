﻿using System.Collections.Generic;
using UnitTestDemo.Model;

namespace UnitTestDemo.Ch3
{
    public interface IItemDA
    {
        List<Item> GetItems();
    }
}