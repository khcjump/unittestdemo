﻿namespace UnitTestDemo.Model
{
    public class Item
    {
        public string ItemNumber { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UnitPrice { get; set; }
        public string ExtendPrice { get; set; }
        public string Qty { get; set; }
        public string Label { get; set; }
    }
}