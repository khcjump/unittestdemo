﻿using Ch1;
using Xunit;

namespace UnitTestDemo.Tests.Ch1
{
    public class ItemBusinessTests
    {
        [Theory]
        [InlineData("Iphone", 1, 10)]
        [InlineData("Samsung", 1, 0)]
        [InlineData("Iphone", 6, 60)]
        [InlineData("Samsung", 6, 0)]
        public void GetExtendPrices_InputMultipleData_OutputCorrectPrices(string item, int qty, decimal expected)
        {
            //arrange
            var itemBusiness = new ItemBusiness();

            //act
            var actual = itemBusiness.GetExtendPrices(item, qty);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDefaultPrice_InputIphone_OutputTenPrices()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            const string item = "Iphone";
            const decimal expected = 10m;

            //act
            var actual = itemBusiness.GetDefaultPrice(item);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDefaultPrice_InputSamsung_OutputZeroPrice()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            const string item = "Samsung";
            const decimal expected = 0;

            //act
            var actual = itemBusiness.GetDefaultPrice(item);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrices_InputIphoneAndQtyIsSix_OutputSixtyPrices()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            const string item = "Iphone";
            const int qty = 6;
            const decimal expected = 60m;

            //act
            var actual = itemBusiness.GetExtendPrices(item, qty);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrices_InputSamsungAndQtyIsSix_OutputZeroPrice()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            const string item = "samsung";
            const int qty = 6;
            const decimal expected = 0m;

            //act
            var actual = itemBusiness.GetExtendPrices(item, qty);

            //assert
            Assert.Equal(expected, actual);
        }
    }
}