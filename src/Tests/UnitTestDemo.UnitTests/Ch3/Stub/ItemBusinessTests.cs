﻿using NSubstitute;
using UnitTestDemo.Ch3.TestDouble;
using Xunit;

namespace UnitTestDemo.Tests.Ch3.Stub
{
    /// <summary>
    ///     Stub 的範例，這邊會發現多了 Return 的方法並且指定回傳結果，而且在 SUT 調用到我們指定的方法時，會發現如果 match 就會與我們當初設定的一樣，並影響 SUT 回傳結果。
    /// </summary>
    public class ItemBusinessTests
    {
        private IPriceCalculator _priceCalculator;
        private IPriceValidator _priceValidator;


        [Fact]
        public void GetDefaultPrice_DefaultPriceIsOne_ReturnOne()
        {
            //arrange
            var expected = 1m;
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceValidator = Substitute.For<IPriceValidator>();
            _priceCalculator.GetDefaultPrice("test").Returns(1m);
            _priceValidator.ValidatePrice(2).Returns(true);
            var itemBusiness = new ItemBusiness(_priceCalculator, _priceValidator);

            //act
            var actual = itemBusiness.GetDefaultPrice("test");

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrice_ExtendPriceIsTwo_ReturnTwo()
        {
            //arrange
            var expected = 2m;
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceValidator = Substitute.For<IPriceValidator>();
            _priceCalculator.GetExtendPrice("test", 2).Returns(2m);
            _priceValidator.ValidatePrice(2).Returns(true);
            var itemBusiness = new ItemBusiness(_priceCalculator, _priceValidator);

            //act
            var actual = itemBusiness.GetExtendPrice("test", 2);

            //assert
            Assert.Equal(expected, actual);
        }
    }
}