﻿using System;
using UnitTestDemo.Ch3.TestDouble;
using Xunit;

namespace UnitTestDemo.Tests.Ch3.Fake
{
    /// <summary>
    ///     Fake 的範例，透過 FakeTimeWrapper 的類別去實作代碼，從 Debug 會發現 dateTimeProvider 將會走到我們實作 FakeTimeWrapper 邏輯
    ///     這邊 Seam 的方式範例就用簡單的塞值去處理而不是建構注入
    /// </summary>
    public class DateTimeProviderTests
    {
        [Fact]
        public void GetCurrentTame_SetMockTime_ReturnMockTime()
        {
            // arrange
            var dateTimeProvider = new DateTimeProvider();
            var timeWrapper = new FakeTimeWrapper {MockTime = Convert.ToDateTime("2019/01/01")};
            var expected = Convert.ToDateTime("2019/01/01");
            dateTimeProvider.TimeWrapper = timeWrapper;

            // act
            var actual = dateTimeProvider.GetCurrentTime();

            // assert
            Assert.Equal(expected, actual);
        }
    }
}