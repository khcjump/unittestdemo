﻿using UnitTestDemo.Ch3.TestDouble;

namespace UnitTestDemo.Tests.Ch3.Spy
{
    public class SpyLogger : ILogger
    {
        private bool _WriteLog = true;

        public void WriteLog(string message)
        {
            _WriteLog = true;
        }

        public bool IsWrittenLog()
        {
            return _WriteLog;
        }
    }
}