﻿using System.Collections.Generic;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using UnitTestDemo.Ch3;
using UnitTestDemo.Model;
using Xunit;

namespace UnitTestDemo.Tests.Ch3
{
    public class ItemBusinessTests
    {
        private IPriceCalculator _priceCalculator;
        private IItemDA _itemDa;

        [Fact]
        public void GetAppleItems_AppleItemTwoCounts_ReturnCorrectValues()
        {
            //arrange
            var items = new List<Item>
            {
                new Item {Label = "Apple", ItemNumber = "1"},
                new Item {Label = "Samsung", ItemNumber = "2"},
                new Item {Label = "Apple", ItemNumber = "3"}
            };

            var expected = new List<Item>
            {
                new Item {Label = "Apple", ItemNumber = "1"},
                new Item {Label = "Apple", ItemNumber = "3"}
            };

            _itemDa = Substitute.For<IItemDA>();
            _itemDa.GetItems().Returns(items);
            var itemBusiness = new ItemBusiness(_priceCalculator, _itemDa);

            //act
            var actual = itemBusiness.GetAppleItems();

            //assert
            _itemDa.Received(1).GetItems();
            Assert.Equal(expected.Count, actual.Count);
            Assert.Equal(expected[0].ItemNumber, actual[0].ItemNumber);
            Assert.Equal(expected[0].Label, actual[0].Label);
            Assert.Equal(expected[1].ItemNumber, actual[1].ItemNumber);
            Assert.Equal(expected[1].Label, actual[1].Label);
        }


        [Fact]
        public void GetAppleItems_itemsIsNullOrEmpty_ReturnNull()
        {
            //arrange
            _itemDa = Substitute.For<IItemDA>();
            _itemDa.GetItems().ReturnsNull();
            var itemBusiness = new ItemBusiness(_priceCalculator, _itemDa);

            //act
            var actual = itemBusiness.GetAppleItems();

            //assert
            _itemDa.Received(1).GetItems();
            Assert.Null(actual);
        }

        [Fact]
        public void GetDefaultPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            var expected = 1m;
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceCalculator.GetDefaultPrice(Arg.Any<string>()).Returns(1m);
            var itemBusiness = new ItemBusiness(_priceCalculator, _itemDa);

            //act
            var actual = itemBusiness.GetDefaultPrice(string.Empty);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            var expected = 0m;
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceCalculator.GetExtendPrices(Arg.Any<string>(), Arg.Any<int>()).Returns(0m);
            var itemBusiness = new ItemBusiness(_priceCalculator, _itemDa);

            //act
            var actual = itemBusiness.GetExtendPrices(string.Empty, 0);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetItems_CheckDependOnItemDA_CallOneTime()
        {
            //arrange
            _itemDa = Substitute.For<IItemDA>();
            var itemBusiness = new ItemBusiness(_priceCalculator, _itemDa);

            //act
            itemBusiness.GetItems();

            //assert
            _itemDa.Received(1).GetItems();
        }
    }
}