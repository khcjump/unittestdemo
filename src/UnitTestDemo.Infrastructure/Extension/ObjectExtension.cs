﻿namespace UnitTestDemo.Infrastructure.Extension
{
    public static class ObjectExtension
    {
        public static bool IsNull(this object source)
        {
            return source == null;
        }

        public static bool IsNotNull(this object source)
        {
            return source.IsNull() == false;
        }
    }
}