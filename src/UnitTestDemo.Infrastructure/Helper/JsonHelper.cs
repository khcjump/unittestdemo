﻿using System.IO;
using Newtonsoft.Json;

namespace UnitTestDemo.Infrastructure.Helper
{
    public class JsonHelper
    {
        public static T LoadJson<T>(string path)
        {
            using (var r = new StreamReader(path))
            {
                var json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(json);
            }
        }
    }
}