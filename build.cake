#tool "nuget:?package=ReportGenerator&&version=4.1.4"

var target = Argument("target", "Clean");
//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Clean")
    .Does(() =>
{
    CleanDirectories("./src/**/bin/");
    CleanDirectories("./src/**/obj");
});

Task("Restore-NuGet-Packages")
    .IsDependentOn("Clean")
    .Does(() =>
{
    DotNetCoreRestore("./UnitTestDemo.sln", new DotNetCoreRestoreSettings
    {
        Verbosity = DotNetCoreVerbosity.Minimal,
        Sources = new [] { "https://api.nuget.org/v3/index.json" },
    });
});

Task("Build")
    .IsDependentOn("Restore-NuGet-Packages")
    .Does(() =>
{
    // Build the solution.
    var path = MakeAbsolute(new DirectoryPath("./UnitTestDemo.sln"));
    DotNetCoreBuild(path.FullPath, new DotNetCoreBuildSettings()
    {
        NoRestore = true,
    });
});


Task("Run-Unit-Tests")
    .IsDependentOn("Build")
    .Does(() =>
{
    var projects = GetFiles("./src/Tests/**/*.UnitTests.csproj");
    foreach(var project in projects)
    {
        var projectName = project.GetFilenameWithoutExtension().FullPath;

        FilePath
            testResultsCore = $"./{projectName}_core_TestResults.xml";
            //MakeAbsolute($"./{projectName}_core_TestResults.xml");

        // .NET Core
        DotNetCoreTest(project.FullPath, new DotNetCoreTestSettings
        {
            Framework = "netcoreapp2.1",
            NoBuild = true,
            NoRestore = true,
            ArgumentCustomization = args=>args.Append($"--logger trx;LogFileName=\"./{testResultsCore}\"")
        });
    }
});

 
Task("ReportGenerator")
    //.IsDependentOn("Run-Unit-Tests")
    .Does(() => 
    {
        Information("Finished running tasks.");
        var reportGeneratorSettings = new ReportGeneratorSettings();
        reportGeneratorSettings.HistoryDirectory = new DirectoryPath("./src/test/TestResults"); 
        Information("Finished running tasks.");
         var test = GetFiles("./src/test/TestResult/**.xml");
    
        ReportGenerator("D:/git-repository/unittestdemo/src/Tests/UnitTestDemo.UnitTests/TestResults/UnitTestDemo.UnitTests_core_TestResults_2019-04-28_23-34-01-283.xml", "D:/git-repository/unittestdemo/src/Tests");
    });

Information("Finished running tasks.");



RunTarget("ReportGenerator");